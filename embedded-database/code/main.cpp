/*
 * Started by �ngel in march of 2019
 *
 * This is free software released into the public domain.
 *
 * angel.rodriguez@esne.edu
 */

#include <ciso646>
#include <iostream>
#include <sqlite3.h>
#include <stdexcept>
#include <string>

using std::cout;
using std::cerr;
using std::endl;
using std::string;

void throw_database_error (const string & message, sqlite3 * database)
{
    throw std::runtime_error(message + ":\n" + sqlite3_errmsg (database));
}

bool table_exists (sqlite3 * database, const std::string & table_name)
{
    sqlite3_stmt * query;

    auto query_string = "SELECT name FROM sqlite_master WHERE type='table' AND name='{?}';";

    auto result  = sqlite3_prepare_v2 (database, query_string, -1, &query, 0);

    if  (result != SQLITE_OK)
    {
        throw_database_error ("Database error", database);
    }
    else
    {
        //sqlite3_bind_text (query, 0, table_name.c_str ());
        
        
    }

    return false;
}

void create_users_table ()
{
    
}

int main ()
{
    sqlite3 * database = nullptr;

    auto result = sqlite3_open ("local.db", &database);

    if (result != SQLITE_OK)
    {
        throw_database_error ("Failed to open/create the database", database);
    }
    else try
    {
        if (not table_exists (database, "users"))
        {
            create_users_table ();
        }


    }
    catch (const std::exception & error)
    {
        cerr << error.what () << endl;
    }

    if (database)
    {
        sqlite3_close (database);
    }

    return 0;
}
