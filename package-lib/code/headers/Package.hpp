/*
 *  Coded by �ngel in March 2019.
 *
 *  This is free software released into the public domain.
 *
 *  angel.rodriguez@esne.edu
 */

#pragma once

#include <cstddef>
#include <cstdint>
#include <fstream>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace example
{

    using std::ifstream;
    using std::string;
    using std::unique_ptr;

    class Package
    {
    private:

        /** Guarda la informaci�n de un archivo empaquetado.
          */
        struct Entry
        {
            uint32_t offset;
            uint32_t size;
        };

    public:

        /** Permite acceder al contenido de un archivo empaquetado.
          */
        class File
        {
            // En este caso friend mejora la encapsulaci�n al permitir hacer privado el constructor
            // de File de modo que solo se pueda instanciar en Package (con su m�todo get_file()).

            friend class Package;

            string   path;                      ///< Ruta del archivo al que se tiene acceso
            ifstream stream;                    ///< Stream que permite la lectura del contenido
            uint32_t start;                     ///< Offset en el que empieza el contenido del archivo
            uint32_t end;                       ///< Offset en el que termina el contenido del archivo
            uint32_t pointer;                   ///< Offset en el que se encuentra el puntero de lectura

        private:

            File(const string & package_path, const string & file_path, uint32_t offset, uint32_t size);

        public:

            /** Retorna la ruta relativa completa del archivo incluyendo el nombre.
              */
            const string & get_path () const
            {
                return path;
            }

            /** Retorna la ruta relativa completa del archivo sin incluir el nombre.
              */
            string get_base_path () const
            {
                size_t base_end  = path.find_last_of ('/');
                return base_end != string::npos ? path.substr (0, base_end) : string();
            }

            /** Retorna el tama�o del contenido del archivo.
              */
            uint32_t size () const
            {
                return end - start;
            }

            bool good () const { return stream.good (); }
            bool bad  () const { return stream.bad  (); }
            bool eof  () const { return pointer >= end; }

            /** Mueve el puntero de lectura a la posici�n indicada desde el inicio del archivo.
              */
            bool seek (uint32_t offset_from_start);

            /** Retorna un byte le�do del archivo o -1 si no se ha podido leer.
              */
            int get ();

            /** Intenta leer 'size' bytes dentro del buffer proporcionado.
              * Retorna el n�mero de bytes le�dos.
              */
            uint32_t read (uint8_t * buffer, uint32_t size);

            /** Intenta leer 'size' bytes dentro del buffer proporcionado.
              * Retorna el n�mero de bytes le�dos.
              */
            bool read_all (std::vector< uint8_t > & output);

        };

        /** Permite iterar las rutas de los archivos empaquetados.
          */
        class Iterator
        {
            // En este caso friend mejora la encapsulaci�n al permitir hacer privado el constructor
            // de Iterator de modo que solo se pueda instanciar en Package (con su m�todos begin()/end()).

            friend class Package;

            std::map< string, Entry >::const_iterator iterator;

        private:

            Iterator(std::map< string, Entry >::const_iterator start)
            :
                iterator(start)
            {
            }

        public:

            Iterator & operator ++ ()
            {
                return ++iterator, *this;
            }

            Iterator & operator -- ()
            {
                return --iterator, *this;
            }

            const string & operator * () const
            {
                return iterator->first;
            }

            const string * operator -> () const
            {
                return &iterator->first;
            }

            bool operator == (const Iterator & other)
            {
                return this->iterator == other.iterator;
            }

            bool operator != (const Iterator & other)
            {
                return this->iterator != other.iterator;
            }

        };

    public:

        /** Este m�todo permite crear un paquete a partir de su ruta. Si el paquete inidicado no se
          * ha podido decodificar, devuelve un puntero nulo.
          */
        static unique_ptr< Package > open (const string & path);

    private:

        typedef std::map< string, Entry > Entry_Map;

        Entry_Map entry_map;
        string    package_path;
        bool      loaded;

    private:

        /** Decodifica el paquete cuya ruta se indica.
          * Para crear instancias de Package se debe llamar al m�todo Package::open().
          */
        Package(const string & package_path)
        :
            package_path(package_path)
        {
            loaded = decode_header (package_path);
        }

    public:

        /** Retorna un puntero a un objeto de tipo Package::File si la ruta correspondiente se
          * se encuentra en el paquete.
          */
        unique_ptr< File > get_file (const string & path);

        /** Retorna un iterador a la primera ruta contenida en el paquete.
          */
        Iterator begin () const
        {
            return Iterator(entry_map.begin ());
        }

        /** Retorna un iterador a una ruta m�s all� de la �ltima ruta v�lida.
          */
        Iterator end () const
        {
            return Iterator(entry_map.end ());
        }

    private:

        bool     decode_header      (const string & path);
        uint32_t get_header_offset  (ifstream & reader);
        bool     check_mark         (ifstream & reader);
        bool     read_entry         (ifstream & reader);

        bool     read_uint16        (ifstream & reader, uint16_t & value);
        bool     read_uint32        (ifstream & reader, uint32_t & value);
        bool     read_string        (ifstream & reader, string   & value);

    };

}
