/*
 *  Coded by �ngel in March 2019.
 *
 *  This is free software released into the public domain.
 *
 *  angel.rodriguez@esne.edu
 */

#include "../headers/Package.hpp"
#include <cassert>

namespace example
{

    Package::File::File(const string & package_path, const string & file_path, uint32_t offset, uint32_t size)
    :
        path  (file_path),
        stream(package_path, ifstream::binary)
    {
        stream.seekg (pointer = start = offset);

        end = start + size;
    }

    // -------------------------------------------------------------------------------------------------------------- //

    bool Package::File::seek (uint32_t offset_from_start)
    {
        pointer = start + offset_from_start;

        if (pointer > end) pointer = end;

        stream.seekg (pointer);

        return stream.good ();
    }

    // -------------------------------------------------------------------------------------------------------------- //

    int Package::File::get ()
    {
        if (pointer < end)
        {
            pointer++;

            int value = stream.get ();

            if (stream.good ()) return value;
        }

        return -1;
    }

    // -------------------------------------------------------------------------------------------------------------- //

    bool Package::File::read_all (std::vector< uint8_t > & output)
    {
        uint32_t size = end - start;

        if (!seek (0)) return false;

        output.resize (size);

        stream.read (reinterpret_cast< char * >(output.data ()), size);

        pointer = end;

        return stream.good ();
    }

    // -------------------------------------------------------------------------------------------------------------- //

    unique_ptr< Package > Package::open (const string & path)
    {
        unique_ptr< Package > package(new Package(path));

        if (!package->loaded)
        {
             package.reset ();
        }

        return package;
    }

    // -------------------------------------------------------------------------------------------------------------- //

    unique_ptr< Package::File > Package::get_file (const string & file_path)
    {
        unique_ptr< Package::File > file;

        if (entry_map.count (file_path) > 0)
        {
            const Entry & entry = entry_map[file_path];

            file.reset (new File(package_path, file_path, entry.offset, entry.size));
        }

        return file;
    }

    // -------------------------------------------------------------------------------------------------------------- //

    bool Package::decode_header (const string & path)
    {
        // Se abre el archivo del paquete para lectura:

        ifstream reader(path, ifstream::binary);

        if (!reader) return false;

        // Se determina d�nde comienza la cabecera:

        uint32_t header_offset = get_header_offset (reader);

        if (!header_offset) return false;

        // Se mueve el puntero de lectura hacia donde empieza la cacebera:

        reader.seekg (header_offset, ifstream::beg);

        // Se comprueba que al inicio de la cabecera se encuentra la marca del formato:

        if (!check_mark (reader)) return false;

        // Se comprueba la versi�n del formato del paquete:

        int8_t format_version = reader.get ();

        if (format_version != 0) return false;

        // Se lee el n�mero de entradas que contiene el paquete:

        uint32_t number_of_entries = 0;

        if (!read_uint32 (reader, number_of_entries)) return false;

        // Se lee la informaci�n de cada entrada:

        for (uint32_t count = 0; count < number_of_entries; ++count)
        {
            if (!read_entry (reader)) return false;
        }

        return reader.good ();
    }

    // -------------------------------------------------------------------------------------------------------------- //

    uint32_t Package::get_header_offset (ifstream & reader)
    {
        // Se mueve el puntero al principio de los �ltimos 4 bytes:

        reader.seekg (-4, ifstream::end);

        if (!reader.good ()) return 0;

        // Y se lee el entero de 32 bits que se encuentra ah�:

        uint32_t header_start;

        if (!read_uint32 (reader, header_start)) return 0;

        return header_start;
    }

    // -------------------------------------------------------------------------------------------------------------- //

    bool Package::check_mark (ifstream & reader)
    {
        return reader.get () == 'P' && reader.get () == 'K' && reader.get () == 'G' && reader.get () == '!';
    }

    // -------------------------------------------------------------------------------------------------------------- //

    bool Package::read_entry (ifstream & reader)
    {
        string path;
        Entry  entry;

        if
        (
            !read_string (reader, path        ) ||
            !read_uint32 (reader, entry.offset) ||
            !read_uint32 (reader, entry.size  )
        )
        {
            return false;
        }

        entry_map[path] = entry;

        return true;
    }

    // -------------------------------------------------------------------------------------------------------------- //

    bool Package::read_uint16 (ifstream & reader, uint16_t & value)
    {
        value =
            (uint16_t(uint8_t(reader.get ()))     ) |
            (uint16_t(uint8_t(reader.get ())) << 8);

        return !reader.bad ();
    }

    // -------------------------------------------------------------------------------------------------------------- //

    bool Package::read_uint32 (ifstream & reader, uint32_t & value)
    {
        value =
            (uint32_t(uint8_t(reader.get ()))      ) |
            (uint32_t(uint8_t(reader.get ())) <<  8) |
            (uint32_t(uint8_t(reader.get ())) << 16) |
            (uint32_t(uint8_t(reader.get ())) << 24);

        return !reader.bad ();
    }

    // -------------------------------------------------------------------------------------------------------------- //

    bool Package::read_string (ifstream & reader, string & value)
    {
        uint16_t length;

        if (read_uint16 (reader, length))
        {
            value.resize (length);

            reader.read (&value.front (), length);
        }

        return !reader.bad ();
    }

}
