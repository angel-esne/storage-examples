/*
 * Started by �ngel in may of 2017
 *
 * This is free software released into the public domain.
 *
 * angel.rodriguez@esne.edu
 */

#pragma once

#include <string>
#include <fstream>
#include "Output_Stream.hpp"

namespace example
{

    class File_Output_Stream : public Output_Stream
    {
        std::ofstream stream;

    public:

        File_Output_Stream(const std::string & path)
        :
            stream(path, std::ofstream::binary | std::ofstream::trunc)
        {
        }

        bool write (const char * buffer, size_t amount)
        {
            stream.write (buffer, amount);

            return stream.good ();
        }

        bool fail  ()
        {
            return stream.fail ();
        }

    };

}
