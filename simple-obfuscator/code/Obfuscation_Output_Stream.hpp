/*
 * Started by �ngel in may of 2017
 *
 * This is free software released into the public domain.
 *
 * angel.rodriguez@esne.edu
 */

#pragma once

#include <vector>
#include "Output_Stream.hpp"

namespace example
{

    class Obfuscation_Output_Stream : public Output_Stream
    {

        Output_Stream & stream;
        char            key;

    public:

        Obfuscation_Output_Stream(Output_Stream & stream, char key)
        :
            stream(stream),
            key   (key   )
        {
        }

        bool write (const char * buffer, size_t amount)
        {
            std::vector< char > obfuscated_buffer(amount);

            // Se implementa un m�todo de ofuscaci�n trivial y moderadamente eficiente como ejemplo:

            for (size_t i = 0; i < amount; ++i)
            {
                obfuscated_buffer[i] = buffer[i] ^ key;
            }

            return stream.write (obfuscated_buffer.data (), amount);
        }

        bool fail  ()
        {
            return stream.fail ();
        }

    };

}
