/*
 * Started by Ángel in may of 2017
 *
 * This is free software released into the public domain.
 *
 * angel.rodriguez@esne.edu
 */

#include <string>
#include <fstream>
#include <iostream>
#include "File_Output_Stream.hpp"
#include "Obfuscation_Output_Stream.hpp"

using namespace std;
using namespace example;

namespace
{

    void show_help ()
    {
        cout <<
            "Usage:"
            "\tobfuscator input-file-path" << endl;
    }

}

int main (int number_of_arguments, char * arguments[])
{
    // Se validan e interpretan los parámetros de entrada:

    if (number_of_arguments == 1)
    {
        cout << "No se ha especificado un archivo de entrada." << endl;
        show_help ();
        return -1;
    }
    else
    if (number_of_arguments > 2)
    {
        cout << "Se han indicado demasiados argumentos." << endl;
        show_help ();
        return -1;
    }

    // Se intenta abrir el archivo de entrada, crear el de salida y encadenar los streams necesarios:

    string  input_file_path = arguments[1];
    string output_file_path = input_file_path + ".obfuscated";

    ifstream                  reader    ( input_file_path, ifstream::binary);
    File_Output_Stream        writer    (output_file_path);
    Obfuscation_Output_Stream obfuscator(writer, (char)0xAA);

    if (!reader.good ())
    {
        cout << "No se ha podido abrir el archivo de entrada: '" << input_file_path << "'" << endl;
        exit (-1);
    }

    // Se lee el archivo de entrada, se ofusca y se escribe el resultado en el archivo de salida:

    char   buffer[64 * 1024];
    size_t total_bytes_written = 0;

    while (!reader.eof ())
    {
        reader.read (buffer, sizeof(buffer));

        size_t bytes_read = (size_t)reader.gcount ();

        if (reader.bad ())
        {
            cout << "Error leyendo el archivo de entrada." << endl;
            exit (-1);
        }

        obfuscator.write (buffer, bytes_read);

        if (writer.fail ())
        {
            cout << "Error escribiendo el archivo de salida." << endl;
            exit (-1);
        }

        total_bytes_written += bytes_read;
    }

    cout << "Se ha finalizado la ofuscación de " << total_bytes_written << " bytes." << endl;

    return 0;
}
