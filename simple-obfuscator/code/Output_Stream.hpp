/*
 * Started by �ngel in may of 2017
 *
 * This is free software released into the public domain.
 *
 * angel.rodriguez@esne.edu
 */

#pragma once

#include <cstddef>

namespace example
{

    class Output_Stream
    {
    public:

        virtual bool write (const char * buffer, size_t amount) = 0;
        virtual bool fail  () = 0;

    };

}
