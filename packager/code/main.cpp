/*
 *  Coded by �ngel in March 2019.
 *
 *  This is free software released into the public domain.
 *
 *  angel.rodriguez@esne.edu
 */

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <vector>

using namespace std;

namespace
{

    /** Informaci�n que se extrae de los argumentos de entrada de la aplicaci�n.
      */
    struct Parameters
    {
        string  input_path;                     ///< Ruta de la carpeta en la que se buscar�n los archivos a meter en el paquete
        string output_path;                     ///< Ruta del paquete que se crear�
    };

    /** Informaci�n de un archivo empaquetado.
      */
    struct File_Information
    {
        filesystem::path path;                  ///< Ruta del archivo en el sistema de archivos
        string           location;              ///< Ruta del archivo en el paquete
        uint64_t         size;                  ///< Tama�o del archivo en bytes
        uint64_t         offset;                ///< Posici�n del primer byte del archivo en el paquete
    };

    /** Termina la ejecuci�n del programa abruptamente mostrando un mensaje.
      */
    void exit (const string & message)
    {
        cout << message << '\n';

        std::exit (-1);
    }

    /** Termina la ejecuci�n del programa abruptamente mostrando la ayuda y, opcionalmente, un mensaje.
      */
    void show_help_and_exit (const string & message = string())
    {
        if (!message.empty ())
        {
            cout << message << "\n\n";
        }

        cout <<
            "Usage:\n\n"
            "    packager [--help] --input input_path --output output_path\n\n"
            "        --help   Shows this help.\n"
            "        --input  Specifies the input folder path.\n"
            "        --output Specifies the output package file path and name.\n" << endl;

        std::exit (message.empty () ? 0 : -1);
    }

    /** Guarda un valor de un par�metro de entrada tomado de un argumento de la aplicaci�n.
      */
    void set_value (string & target, const string & value)
    {
        // Se comprueba si ya se ha asignado un valor al target anteriormente:

        if (!target.empty ())
        {
            show_help_and_exit ("ERROR: duplicate parameter.");
        }

        // Se comprueba si el valor es el nombre de un argumento, cosa que no deber�a ser:

        if (value.find_first_of ("--") == 0)
        {
            show_help_and_exit ("ERROR: expected a value not starting with --.");
        }

        // Se guarda el valor:

        target = value;
    }

    /** Extrae par�metros de entrada a partir de los argumentos de la aplicaci�n.
      */
    Parameters parse_arguments (int number_of_arguments, char * arguments[])
    {
        Parameters parameters;

        for (int  index = 1; index < number_of_arguments; )
        {
            const string argument = arguments[index++];

            if (argument == "--input")
            {
                if (index == number_of_arguments) show_help_and_exit ("ERROR: expected a path after --input.");

                string path = arguments[index++];

                if (path.back () != '\\' && path.back () != '/') path.push_back (filesystem::path::preferred_separator);

                if (!filesystem::is_directory (path))
                {
                    exit ("ERROR: the input path does not point to a valid folder.");
                }

                set_value (parameters.input_path, path);
            }
            else
            if (argument == "--output")
            {
                if (index == number_of_arguments) show_help_and_exit ("ERROR: expected a path after --output.");

                set_value (parameters.output_path, arguments[index++]);
            }
            else
            if (argument == "--help")
            {
                show_help_and_exit ();
            }
            else
            {
                show_help_and_exit (string("ERROR: unknown argument (\"") + argument + "\").");
            }
        }

        if (parameters. input_path.empty ()) show_help_and_exit ("ERROR: expected an input path." );
        if (parameters.output_path.empty ()) show_help_and_exit ("ERROR: expected an output path.");

        return parameters;
    }

    /** Recopila y devuelve todos los archivos que se encuentran en una carpeta realizando una b�squeda recursiva.
      */
    set< filesystem::path > collate_input_files (const string & input_path)
    {
        set< filesystem::path > paths;

        if (filesystem::exists (input_path) == false)
        {
            exit ("ERROR: the input path doesn't exist.");
        }

        if (filesystem::is_directory (input_path) == false)
        {
            exit ("ERROR: the input path is not a directory.");
        }

        for (const auto & entry : filesystem::recursive_directory_iterator (input_path))
        {
            if (filesystem::is_regular_file (entry))
            {
                if (entry.path ().string ().length () >= 1 << 16)
                {
                    exit ("ERROR: the relative path depth of some files is too long.");
                }

                paths.insert (entry.path ());
            }
        }

        return paths;
    }

    uint32_t write_uint16 (ofstream & package_file, uint16_t value)
    {
        package_file
            .put (char(value      ))
            .put (char(value >>  8));

        return package_file.good () ? 2 : 0;
    }

    uint32_t write_uint32 (ofstream & package_file, uint32_t value)
    {
        package_file
            .put (char(value      ))
            .put (char(value >>  8))
            .put (char(value >> 16))
            .put (char(value >> 24));

        return package_file.good () ? 4 : 0;
    }

    uint32_t write_string (ofstream & package_file, const string & value)
    {
        if (write_uint16 (package_file, uint16_t(value.length ())))
        {
            package_file.write (value.data (), value.length ());

            if (package_file.good ())
            {
                return uint32_t( sizeof(uint16_t) + value.length () );
            }
        }

        return 0;
    }

    /** Escribe en el paquete el contenido de un archivo.
      */
    bool write_file (ofstream & package_file, const filesystem::path & path)
    {
        cout << "Packaging " << path.string () << endl;

        // Se abre el archivo cuyo contenido se va a copiar:

        ifstream  file_reader(path, ifstream::binary);

        // Se prepara el buffer de lectura:

        char   buffer[64 * 1024];
        size_t total_copied = 0;

        while (!file_reader.eof ())
        {
            // Se lee un bloque de datos del archivo de entrada:

            file_reader.read (buffer, sizeof(buffer));

            if (file_reader.bad ())
            {
                return false;
            }

            // Se escribe el bloque de datos en el archivo del paquete:

            package_file.write (buffer, file_reader.gcount ());

            if (!package_file.good ())
            {
                return false;
            }

            // Se contabiliza la cantidad de bytes copiados:

            total_copied += file_reader.gcount ();
        }

        // Todo habr� ido bien si se han conseguido copiar tantos bytes como tenga el archivo de entrada:

        return total_copied = filesystem::file_size (path);
    }

    /** Escribe en el paquete el contenido de todos los archivos que se recopilaron de la carpeta
      * de entrada.
      */
    bool write_package_content (ofstream & package_file, const string & input_path, const set< filesystem::path > & input_files, vector< File_Information > & content_info)
    {
        size_t   prefix =   input_path.length ();
        uint64_t offset = package_file.tellp  ();

        for (const auto & file_path : input_files)
        {
            // Se recopila la informaci�n del archivo:

            File_Information file_info;

            file_info.path     = file_path;
            file_info.location = file_path.string ().substr (prefix);
            file_info.offset   = offset;
            file_info.size     = filesystem::file_size (file_path);

            // Se comprueba que el tama�o del archivo a empaquetar no sea demasiado grande:

            if (file_info.size >= 1L << 31) exit ("ERROR: can't package a file bigger than 2 GB.");

            // Se normalizan los separadores de la ruta del archivo:

            replace (file_info.location.begin (), file_info.location.end (), '\\', '/');

            // Se copia el contenido del archivo en el paquete:

            if (file_info.size > 0 && !write_file (package_file, file_path) || !package_file.good ())
            {
                return false;
            }

            // Se avanza el offset y se guarda la informaci�n del archivo:

            offset += file_info.size;

            if (offset >= 1L << 31) exit ("ERROR: the package has exceeded 2 GB.");

            content_info.push_back (file_info);
        }

        return true;
    }

    /** Escribe en el paquete la informaci�n que describe su contenido.
      */
    bool write_package_header (ofstream & package_file, const vector< File_Information > & content_info)
    {
        cout << "Closing package..." << endl;

        uint32_t header_start = uint32_t(package_file.tellp ());

        // Se escribe la marca identificadora del paquete (CHAR x 4 = "PKG!"):

        package_file.write ("PKG!", 4);

        if (!package_file.good ()) return false;

        // Se escribe la versi�n del formato de paquete (INT8 = 0):

        package_file.put (0);

        if (!package_file.good ()) return false;

        // Se escribe el n�mero de archivos contenidos (UINT32):

        if (!write_uint32 (package_file, uint32_t(content_info.size ()))) return false;

        // Se escribe la informaci�n de cada archivo:

        for (const auto & file_info : content_info)
        {
            // Se escribe la cadena de la ruta del archivo (UINT16(length) + CHAR x length):

            if (!write_string (package_file, file_info.location)) return false;

            // Se escribe el offset que marca el inicio del contenido del archivo (UINT32):

            if (!write_uint32 (package_file, uint32_t(file_info.offset))) return false;

            // Se escribe el tama�o del archivo (UINT32):

            if (!write_uint32 (package_file, uint32_t(file_info.size))) return false;
        }

        // Por �ltimo, se escribe el tama�o de la cabecera (UINT32):

        write_uint32 (package_file, header_start);

        return package_file.good ();
    }

}

int main (int number_of_arguments, char * arguments[])
{
    cout <<
        "PACKAGER\n"
        "Example by Angel released into the public domain in March 2019.\n" << endl;

    // Si no hay argumentos se muestra la ayuda y se termina:

    if (number_of_arguments == 1)
    {
        show_help_and_exit ();
    }

    // Se intenta extraer de la lista de argumentos la ruta de la carpeta de entrada y la del
    // paquete de salida:

    Parameters parameters = parse_arguments (number_of_arguments, arguments);

    // Se buscan todos los archivos que haya en la carpeta de entrada recursivamente:

    auto input_files = collate_input_files (parameters.input_path);

    if  (input_files.empty ())
    {
        exit ("The input folder has no files. No package was created.");
    }

    // Se crea un vector que contendr� la informaci�n de los archivos empaquetados:

    vector< File_Information > content_info;

    content_info.reserve (input_files.size ());

    // Se intenta crea el archivo del paquete de salida:

    cout << "Creating package..." << endl;

    ofstream package_file(parameters.output_path, ofstream::binary | ofstream::trunc);

    if (!package_file.good ())
    {
        exit ("ERROR: couldn't create the output package file.");
    }

    // Se escribe el contenido del paquete:

    if
    (
        !write_package_content (package_file, parameters.input_path, input_files, content_info) ||
        !write_package_header  (package_file, content_info)
    )
    {
        exit ("ERROR: there was a problem writing into the package.");
    }

    cout << "Done.\n" << endl;

    return 0;
}
