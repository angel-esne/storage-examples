/*
 *  Coded by �ngel in March 2019.
 *
 *  This is free software released into the public domain.
 *
 *  angel.rodriguez@esne.edu
 */

#include <Package.hpp>

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <vector>

using namespace std;
using namespace example;

namespace
{

    /** Informaci�n que se extrae de los argumentos de entrada de la aplicaci�n.
      */
    struct Parameters
    {
        string  input_path;                     ///< Ruta del paquete cuyo contenido se extraer�
        string output_path;                     ///< Ruta de la carpeta en la que se extraer� el contenido
    };

    /** Termina la ejecuci�n del programa abruptamente mostrando un mensaje.
      */
    void exit (const string & message)
    {
        cout << message << '\n';

        std::exit (-1);
    }

    /** Termina la ejecuci�n del programa abruptamente mostrando la ayuda y, opcionalmente, un mensaje.
      */
    void show_help_and_exit (const string & message = string())
    {
        if (!message.empty ())
        {
            cout << message << "\n\n";
        }

        cout <<
            "Usage:\n\n"
            "    unpacker [--help] --input input_path --output output_path\n\n"
            "        --help   Shows this help.\n"
            "        --input  Specifies the input package file path and name.\n"
            "        --output Specifies the output folder path.\n" << endl;

        std::exit (message.empty () ? 0 : -1);
    }

    /** Guarda un valor de un par�metro de entrada tomado de un argumento de la aplicaci�n.
      */
    void set_value (string & target, const string & value)
    {
        // Se comprueba si ya se ha asignado un valor al target anteriormente:

        if (!target.empty ())
        {
            show_help_and_exit ("ERROR: duplicate parameter.");
        }

        // Se comprueba si el valor es el nombre de un argumento, cosa que no deber�a ser:

        if (value.find_first_of ("--") == 0)
        {
            show_help_and_exit ("ERROR: expected a value not starting with --.");
        }

        // Se guarda el valor:

        target = value;
    }

    /** Extrae par�metros de entrada a partir de los argumentos de la aplicaci�n.
      */
    Parameters parse_arguments (int number_of_arguments, char * arguments[])
    {
        Parameters parameters;

        for (int  index = 1; index < number_of_arguments; )
        {
            const string argument = arguments[index++];

            if (argument == "--input")
            {
                if (index == number_of_arguments) show_help_and_exit ("ERROR: expected a path after --input.");

                set_value (parameters.input_path, arguments[index++]);
            }
            else
            if (argument == "--output")
            {
                if (index == number_of_arguments) show_help_and_exit ("ERROR: expected a path after --output.");

                string path = arguments[index++];

                // Se quitan todas las barras separadoras del final:

                while (path.back () == '\\' || path.back () == '/')
                {
                    path.pop_back ();
                }

                set_value (parameters.output_path, path);
            }
            else
            if (argument == "--help")
            {
                show_help_and_exit ();
            }
            else
            {
                show_help_and_exit (string("ERROR: unknown argument (\"") + argument + "\").");
            }
        }

        if (parameters. input_path.empty ()) show_help_and_exit ("ERROR: expected an input path." );
        if (parameters.output_path.empty ()) show_help_and_exit ("ERROR: expected an output path.");

        return parameters;
    }

    void extract (Package::File & file, const string & output_folder_path, vector< uint8_t > & buffer)
    {
        // Se lee todo el contenido del archivo en el buffer:

        if (!file.read_all (buffer))
        {
            exit ("ERROR: failed to read the content from the package.");
        }

        // Se crea el �rbol de subcarpetas en el que reside el archivo si no existe de antemano:

        string base_path = output_folder_path + file.get_base_path ();

        if (!filesystem::exists (base_path))
        {
            if (!filesystem::create_directories (base_path))
            {
                exit ("ERROR: failed to expand a directory.");
            }
        }

        // Se crea/sobrescribe el archivo extra�do:

        ofstream writer(output_folder_path + file.get_path (), ofstream::binary | ofstream::trunc);

        if (!writer)
        {
            exit ("ERROR: failed to create the output file.");
        }

        // Se escribe el contenido del archivo:

        writer.write (reinterpret_cast< const char * >(buffer.data ()), file.size ());

        if (!writer.good ())
        {
            exit ("ERROR: failed to write in the output file.");
        }
    }

}

int main (int number_of_arguments, char * arguments[])
{
    cout <<
        "UNPACKER\n"
        "Example by Angel released into the public domain in March 2019.\n" << endl;

    // Si no hay argumentos se muestra la ayuda y se termina:

    if (number_of_arguments == 1)
    {
        show_help_and_exit ();
    }

    // Se intenta extraer de la lista de argumentos la ruta del paquete de entrada y la de la
    // carpeta salida:

    Parameters parameters = parse_arguments (number_of_arguments, arguments);

    // Se abre el paquete y se decodifica su cabecera:

    auto package = Package::open (parameters.input_path);

    if (!package)
    {
        exit ("ERROR: failed to open the input package.");
    }

    // Si no existe la carpeta de destino, se intenta crear antes de intentar expandir el contenido
    // del paquete. Nota: si la ruta termina en separador, la funci�n create_directories() fallar�.

    if (!filesystem::exists (parameters.output_path))
    {
        cout << "Creating the output folder..." << endl;

        if (!filesystem::create_directories (parameters.output_path))
        {
            exit ("ERROR: failed to create the output folder.");
        }
    }

    // Se crea un buffer en el que se leer� el contenido de cada archivo antes de escribirlo.
    // Se crea aqu� para que lo compartan todos los archivos.

    std::vector< uint8_t > buffer;

    // Se a�ade un separador a la carpeta de destino para facilitar la concatenaci�n:

    parameters.output_path.push_back (filesystem::path::preferred_separator);

    // Se itera por los archivos contenidos en el paquete y se extraen uno a uno:

    for (auto & path : *package)
    {
        cout << "Extracting " << path << "..." << endl;

        extract (*package->get_file (path), parameters.output_path, buffer);
    }

    cout << "Done.\n" << endl;

    return 0;
}
